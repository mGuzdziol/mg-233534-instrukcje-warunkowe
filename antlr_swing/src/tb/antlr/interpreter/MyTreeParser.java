package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	
	public GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(int a, int b) {
		return a + b;
	}
	
	protected Integer sub(int a, int b) {
		return a - b;
	}
	
	protected Integer mul(int a, int b) {
		return a*b;
	}
	
	protected Integer div(int a, int b) {
		if(b==0) {
			System.out.println("Nie można dzielić przez 0!");
		}
		
		return a/b;
	}
}
